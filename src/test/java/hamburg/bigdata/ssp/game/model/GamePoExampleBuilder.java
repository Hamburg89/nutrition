package hamburg.bigdata.ssp.game.model;

public class GamePoExampleBuilder {

    public static GamePo createDefaultGamePo() {
        GamePo gamePo = new GamePo();
        gamePo.setComputerChoice(PieceType.Papier);
        gamePo.setHumanChoice(PieceType.Schere);
        gamePo.setResult(GameResult.Sieg);
        return gamePo;
    }
}
