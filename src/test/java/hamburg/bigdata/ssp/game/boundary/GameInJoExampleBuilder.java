package hamburg.bigdata.ssp.game.boundary;

import hamburg.bigdata.ssp.game.model.PieceType;

public class GameInJoExampleBuilder {

    public static GameInJo createDefaultGameInJo() {
        GameInJo gameInJo = new GameInJo();
        gameInJo.setHumanChoice(PieceType.Schere);
        return gameInJo;
    }
}
