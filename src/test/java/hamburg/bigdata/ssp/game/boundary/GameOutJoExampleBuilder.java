package hamburg.bigdata.ssp.game.boundary;

import hamburg.bigdata.ssp.game.model.GameResult;
import hamburg.bigdata.ssp.game.model.PieceType;

public class GameOutJoExampleBuilder {

    public static GameOutJo createDefaultGameOutJo() {
        GameOutJo gameOutJo = new GameOutJo();
        gameOutJo.setId(1);
        gameOutJo.setComputerChoice(PieceType.Papier);
        gameOutJo.setHumanChoice(PieceType.Schere);
        gameOutJo.setResult(GameResult.Sieg);
        return gameOutJo;
    }
}
