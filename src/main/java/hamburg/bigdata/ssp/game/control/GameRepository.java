package hamburg.bigdata.ssp.game.control;

import hamburg.bigdata.ssp.game.model.GamePo;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GameRepository extends MongoRepository<GamePo, String> {
}
