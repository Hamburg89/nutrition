package hamburg.bigdata.ssp.game.control;

import hamburg.bigdata.ssp.game.boundary.GameOutJo;
import hamburg.bigdata.ssp.game.boundary.GameOutJoMapper;
import hamburg.bigdata.ssp.game.model.GamePo;
import hamburg.bigdata.ssp.util.GameUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@Slf4j
public class GameService {

    private final GameRepository gameRepo;
    private final GameUtil gameUtil;

    @Autowired
    public GameService(GameRepository gameRepo, GameUtil gameUtil) {
        this.gameRepo = gameRepo;
        this.gameUtil = gameUtil;
    }

    public GamePo playGame(GamePo game) {
        game = this.save(initGame(game));
        return game;
    }

    private GamePo initGame(GamePo game) {
        return this.gameUtil.initValues(game);
    }

    public GamePo save(GamePo game) {
        if (game == null) {
            throw new IllegalArgumentException("Invalid game param");
        }
        return gameRepo.save(game);
    }

    public List<GameOutJo> getAllGames() {
        List<GameOutJo> games = new LinkedList<>();
        gameRepo.findAll().forEach(gamePo -> games.add(GameOutJoMapper.map(gamePo)));
        int index = 1;
        for (GameOutJo game : games) {
            game.setId(index++);
        }
        return games;
    }


}
