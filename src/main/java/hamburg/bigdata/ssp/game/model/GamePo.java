package hamburg.bigdata.ssp.game.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = GamePo.GAME)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class GamePo implements Serializable {

    private static final long serialVersionUID = 6231223365093172636L;
    /**
     * name of collection game
     */
    public static final String GAME = "game";

    private PieceType computerChoice;
    private PieceType humanChoice;
    private GameResult result;

}
