package hamburg.bigdata.ssp.game.model;

public enum PieceType {
    Stein,
    Schere,
    Papier
}
