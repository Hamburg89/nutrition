package hamburg.bigdata.ssp.game.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Piece {
    private PieceType name;

    private PieceType wins;
    private PieceType loses;


}
