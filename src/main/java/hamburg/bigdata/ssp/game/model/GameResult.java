package hamburg.bigdata.ssp.game.model;

public enum GameResult {
    Sieg,
    Niederlage,
    Unentschieden
}
