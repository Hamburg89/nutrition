package hamburg.bigdata.ssp.game.boundary;

import hamburg.bigdata.ssp.game.model.GamePo;

public class GameInJoMapper {

    public static GamePo map(GameInJo source) {
        if (source == null) {
            return null;
        }
        GamePo target = new GamePo();
        target.setHumanChoice(source.getHumanChoice());
        return target;
    }
}
