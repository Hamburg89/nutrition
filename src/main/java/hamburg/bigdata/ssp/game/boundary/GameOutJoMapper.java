package hamburg.bigdata.ssp.game.boundary;

import hamburg.bigdata.ssp.game.model.GamePo;

public class GameOutJoMapper {

    public static GameOutJo map(GamePo source) {
        if (source == null) {
            return null;
        }
        GameOutJo target = new GameOutJo();
        target.setComputerChoice(source.getComputerChoice());
        target.setHumanChoice(source.getHumanChoice());
        target.setResult(source.getResult());
        return target;
    }
}
