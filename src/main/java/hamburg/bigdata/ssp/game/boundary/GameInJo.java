package hamburg.bigdata.ssp.game.boundary;

import com.fasterxml.jackson.annotation.JsonInclude;
import hamburg.bigdata.ssp.game.model.PieceType;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@NoArgsConstructor
public class GameInJo {

    @NotNull
    private PieceType humanChoice;

}
