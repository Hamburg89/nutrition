package hamburg.bigdata.ssp.game.boundary;

import hamburg.bigdata.ssp.game.control.GameRepository;
import hamburg.bigdata.ssp.game.control.GameService;
import hamburg.bigdata.ssp.game.model.GamePo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@Slf4j
@RequestMapping("/api")
public class GameRestController {

	private final GameOutJoMapper gameOutJoMapper = new GameOutJoMapper();

	private final GameInJoMapper gameInJoMapper = new GameInJoMapper();

	private GameService gameSrv;

	@Autowired
    GameRepository repository;

	public GameRestController(GameService gameSrv) {
		this.gameSrv = gameSrv;
	}

	@PostMapping(path = "/game", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<GameOutJo> game(@RequestBody @Valid GameInJo gameInJo) {
		log.info("post() " + gameInJo);
		GamePo game = gameInJoMapper.map(gameInJo);
		GamePo gamePo = gameSrv.playGame(game);
		GameOutJo gameOutJo = gameOutJoMapper.map(gamePo);
		log.info("" + gameOutJo);
		return ResponseEntity.ok().body(gameOutJo);
	}

	@GetMapping(path = "/games", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<List<GameOutJo>> game() {
		List<GameOutJo> gameOutJos = gameSrv.getAllGames();
		log.info("get() " + gameOutJos);
		return ResponseEntity.ok().body(gameOutJos);
	}
}
