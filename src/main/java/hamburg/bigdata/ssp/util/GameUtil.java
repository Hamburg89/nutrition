package hamburg.bigdata.ssp.util;

import hamburg.bigdata.ssp.game.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Random;


@Slf4j
@Component
public class GameUtil {

    private static final PieceType[] PIECE_TYPES = {PieceType.Stein, PieceType.Schere, PieceType.Papier};

    /**
     * Init properties with values
     */
    public GamePo initValues(GamePo game) {
        game.setComputerChoice(generateComputerChoice());
        game.setResult(evaluateGame(game));
        return game;
    }

    private PieceType generateComputerChoice() {
        int idx = new Random().nextInt(PIECE_TYPES.length);
        return (PIECE_TYPES[idx]);
    }

    public GameResult evaluateGame(GamePo game) {
        Piece computer = PieceFactory.create(game.getComputerChoice());
        Piece human = PieceFactory.create(game.getHumanChoice());
        if (computer.getName().equals(human.getWins())) {
            return GameResult.Sieg;
        } else if (computer.getName().equals(human.getLoses())) {
            return GameResult.Niederlage;
        }
        return GameResult.Unentschieden;
    }

}
