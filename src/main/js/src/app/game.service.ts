import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private baseUrl = 'http://localhost:8080/api';

  constructor(private httpClient: HttpClient) { }

  createGame(game: Object): Observable<Object> {
    return this.httpClient.post(`${this.baseUrl}` + `/game`, game);
  }

  getGamesList(): Observable<any> {
    return this.httpClient.get(`${this.baseUrl}` + `/games`);
  }
}
