import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GameplayComponent} from "./gameplay/gameplay.component";
import { ScorelistComponent} from "./scorelist/scorelist.component";

const routes: Routes = [
  { path: '', redirectTo: 'gameplay', pathMatch: 'full' },
  { path: 'gameplay', component: GameplayComponent },
  { path: 'scorelist', component: ScorelistComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
