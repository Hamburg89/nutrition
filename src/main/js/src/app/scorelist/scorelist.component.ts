import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Game} from "../game";
import {GameService} from "../game.service";

@Component({
  selector: 'app-scorelist',
  templateUrl: './scorelist.component.html',
  styleUrls: ['./scorelist.component.css']
})
export class ScorelistComponent implements OnInit {

  games: Observable<Game[]>;

  constructor(private gameService: GameService) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.games = this.gameService.getGamesList();
    this.gameService.getGamesList()
    .subscribe(data => console.log(data), error => console.log(error));;
  }

}
