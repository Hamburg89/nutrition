export class Game {
  id: number;
  computerChoice: string;
  humanChoice: string;
  result: string;
}
